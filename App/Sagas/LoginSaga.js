import { take, put, call } from 'redux-saga/effects'
import Types from '../Actions/Types'
import Actions from '../Actions/Creators'
import API from '../Services/TouchdApi'

// attempts to login
export function * attemptLogin(username, password) {
    const api = API.create();
    //if (password === '') {
        // dispatch failure
        //yield put(Actions.loginFailure('WRONG'))
    //} else {
        // dispatch successful logins
        try {
            const response = yield call (api.registerUser,{
                "device_id": "ffffffff-934a-7b22-ffff-ffff99d603a9",
                "region": "PK",

                "push_token": "fVrXpm6pdsA:APA91bHY84T2dUusz2BLIiCeVQZXu3abRQSKNEJDhvATeKMPKwwt0088Hy2Y3IDKjoHmtThHWB9Yk2V4Fym9jS3ndKDdViqQULq50Wf10arfdjJXs6u6lXLSrcMMBAun4N-dJsimOJ9j",

                "facebook_access_token": "",
                "facebook_email": "",
                "facebook_id": "",
                "facebook_name": "",

                "twitter_email": "",
                "twitter_access_token": "",
                "twitter_id": "",
                "twitter_access_token_secret": ""
            });
            console.log('response : ', response);
            yield put(Actions.loginSuccess(username))
        } catch (error){
            yield put(Actions.loginFailure('WRONG'))
        }
    //}
}

// a daemonized version which waits for LOGIN_ATTEMPT signals
export function * watchLoginAttempt() {
    // daemonize
    while (true) {
        // wait for LOGIN_ATTEMPT actions to arrive
        const { username, password } = yield take(Types.LOGIN_ATTEMPT)
        // call attemptLogin to perform the actual work
        yield call(attemptLogin, username, password)
    }
}
