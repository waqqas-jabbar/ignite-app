import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles } from '../../Themes/'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
    ...ApplicationStyles.screen,
    logo: {
        //height: Metrics.images.logo,
        //width: Metrics.images.logo,
        height: 100,
        width: 100,
        resizeMode: 'contain'
    },
    centered: {
        alignItems: 'center'
    },
    row: {
        paddingVertical: 20,
        paddingHorizontal: 20
    },
    form: {
        backgroundColor: Colors.snow,
        margin: 10,
        borderRadius: 4
    },
    rowLabel: {
        color: Colors.coal,
        paddingHorizontal: 20,
            marginLeft: 30
    },
    textInput: {
        height: 40,
        color: Colors.coal
    },
    textInputReadonly: {
        height: 40,
        color: Colors.steel
    },
    loginRow: {
        paddingBottom: 20,
        paddingHorizontal: 20,
        flexDirection: 'row'
    },
    loginButtonWrapper: {
        flex: 1
    },
    loginButton: {
        flex: 1,
        borderWidth: 1,
        borderColor: Colors.charcoal,
        backgroundColor: Colors.panther,
        padding: 6
    },
    loginText: {
        textAlign: 'center',
        color: Colors.silver
    },
    topLogo: {
        alignSelf: 'center',
        resizeMode: 'contain'
    }
})
