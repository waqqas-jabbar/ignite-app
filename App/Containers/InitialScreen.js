import React, { PropTypes } from 'react'
import { ScrollView, Text, View } from 'react-native'
import { connect } from 'react-redux'
import Actions from '../Actions/Creators'
import Routes from '../Navigation/Routes'

// Styles
import styles from './Styles/InitialScreenStyle'

class InitialScreen extends React.Component {

    constructor(props) {
        super(props)
        this.state = {}
    }

    componentWillMount() {
        const { loggedIn } = this.props;
        const { navigator } = this.props
        if(!loggedIn){
            const route = Routes.LoginScreen
            navigator.push(route)
        }
    }

    static propTypes = {
        navigator: PropTypes.object.isRequired
    }

    checkLogin() {

    }

    render() {
        return (
            <ScrollView style={styles.container}>
                <Text >InitialScreen Container</Text>
            </ScrollView>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        loggedIn: state.login.username !== null
    }
}

export default connect(mapStateToProps)(InitialScreen)
