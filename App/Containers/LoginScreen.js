import React, {PropTypes} from 'react'
import { ScrollView, Text, Image, View, TextInput } from 'react-native'
import { Images } from '../Themes'
import { connect } from 'react-redux'
import Actions from '../Actions/Creators'
import Routes from '../Navigation/Routes'
import RoundedButton from '../Components/RoundedButton'
import I18n from '../I18n/I18n.js'

// Styles
import styles from './Styles/LoginScreenStyle'

import {create} from 'apisauce'
import API from '../Services/TouchdApi'

class LoginScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        };

        this.api = API.create();
    }

    static propTypes = {
        navigator: PropTypes.object.isRequired
    }

    componentWillReceiveProps(nextProps) {
        // Request push premissions only if the user has logged in.
        //console.log(nextProps);
        const { loggedIn } = nextProps
        if (loggedIn) {
            /*
             * If you have turned on Push in Xcode, http://i.imgur.com/qFDRhQr.png
             * uncomment this code below and import at top
             */
            // if (__DEV__) console.log('Requesting push notification permissions.')
            // PushNotification.requestPermissions()
        }
    }

    componentWillMount() {
        this.props.navigator.state.tapHamburger = () => {
            this.props.navigator.drawer.toggle()
        }
    }

    // fires when the user presses the login button
    handlePressSignIn = () => {
        //const { navigator } = this.props
        //const route = Routes.LoginScreen
        //navigator.push(route)
        //navigator.pop()

        const { username, password } = this.state
        const { dispatch } = this.props
        // attempt a login - a saga is listening to pick it up from here.
        dispatch(Actions.attemptLogin(username, password))

        this.api.registerUser({
            "device_id":"ffffffff-934a-7b22-ffff-ffff99d603a9",
            "region":"PK",

            "push_token":"fVrXpm6pdsA:APA91bHY84T2dUusz2BLIiCeVQZXu3abRQSKNEJDhvATeKMPKwwt0088Hy2Y3IDKjoHmtThHWB9Yk2V4Fym9jS3ndKDdViqQULq50Wf10arfdjJXs6u6lXLSrcMMBAun4N-dJsimOJ9j",

            "facebook_access_token":"",
            "facebook_email":"",
            "facebook_id":"",
            "facebook_name":"",

            "twitter_email":"",
            "twitter_access_token":"",
            "twitter_id":"",
            "twitter_access_token_secret":""
        }).then((result) => {
            //console.log(result);
        });

    }

    // fires when the user presses the logout button
    handlePressLogout = () => {
        const { dispatch } = this.props
        dispatch(Actions.logout())
    }

    handleChangeUsername = (text) => {
        this.setState({username: text})
    }

    handleChangePassword = (text) => {
        this.setState({password: text})
    }

    renderLoginButton() {
        return (
            <RoundedButton onPress={this.handlePressSignIn}>
                {I18n.t('signIn')}
            </RoundedButton>
        )
    }

    renderLogoutButton() {
        return (
            <RoundedButton onPress={this.handlePressLogout}>
                {I18n.t('logOut')}
            </RoundedButton>
        )
    }

    renderButtons() {
        const { loggedIn } = this.props
        return (
            <View style={styles.groupContainer}>
                {loggedIn ? this.renderLogoutButton() : this.renderLoginButton()}
            </View>
        )
    }

    render() {
        const { username, password } = this.state
        //const { attempting } = this.props
        //const editable = !attempting
        const editable = true
        const textInputStyle = editable ? styles.textInput : styles.textInputReadonly
        return (
            <View style={styles.mainContainer}>
                <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch'/>
                <ScrollView style={styles.container}>
                    <View style={styles.centered}>
                        <Image source={Images.touch_icon} style={styles.logo}/>
                    </View>

                    <View style={styles.section}>
                        <Text style={styles.sectionText}>
                            Join Touch'd
                        </Text>
                    </View>

                    <View style={styles.form}>
                        <View style={styles.row}>
                            <TextInput
                                ref='username'
                                style={textInputStyle}
                                value={username}
                                editable={editable}
                                keyboardType='default'
                                returnKeyType='search'
                                onChangeText={this.handleChangeUsername}
                                underlineColorAndroid='transparent'
                                placeholder={I18n.t('username')}/>
                        </View>

                        <View style={styles.row}>
                            <TextInput
                                ref='password'
                                style={textInputStyle}
                                value={password}
                                editable={editable}
                                keyboardType='default'
                                returnKeyType='search'
                                secureTextEntry
                                onChangeText={this.handleChangePassword}
                                underlineColorAndroid='transparent'
                                placeholder={I18n.t('password')}/>
                        </View>

                        <RoundedButton onPress={this.handlePressSignIn}>
                            {I18n.t('signIn')}
                        </RoundedButton>

                        <Text style={styles.rowLabel}>{I18n.t('dontHaveAnAccount')}</Text>

                        <RoundedButton onPress={this.handlePressSignIn}>
                            {I18n.t('signUp')}
                        </RoundedButton>
                    </View>

                    <View style={styles.centered}>
                        <Text style={styles.subtitle}>Your not moving ahead!!!</Text>
                    </View>

                </ScrollView>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        loggedIn: state.login.username !== null,
        attempting: state.login.attempting
    }
}

export default connect(mapStateToProps)(LoginScreen)
